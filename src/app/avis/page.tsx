import { ReviewList } from "@/components/ReviewList";

async function getAllReviews() {
  const response = await fetch("http://localhost:3000/api/reviews", {
    method: "GET",
  });

  return response.json();
}

export default async function Avis() {
  const { ratings } = await getAllReviews();

  return <ReviewList reviews={ratings} />;
}
