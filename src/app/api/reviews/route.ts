import { NextResponse } from "next/server";

import data from "../../../seeds/ratings.json";

export async function GET() {
  const ratings = data.feedbacks;
  return NextResponse.json({
    ratings,
  });
}
