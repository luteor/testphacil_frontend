import Link from "next/link";

import { Button } from "@/components/Button";

export default function Home() {
  return (
    <div className="flex min-h-screen flex-col items-center justify-between p-24">
      <Link href="/avis">
        <Button content="Lire tous les avis" variant="primary" />
      </Link>
    </div>
  );
}
