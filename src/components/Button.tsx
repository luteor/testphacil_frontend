type ButtonProps = {
  content: string;
  handleClickButton?: () => void;
  variant: "primary" | "secondary";
};

export const Button = ({
  content,
  handleClickButton,
  variant,
}: ButtonProps) => {
  const getButtonStyle = () => {
    switch (variant) {
      case "primary":
        return "from-phacil-gradient-red to-phacil-gradient-orange rounded bg-gradient-to-br px-10 py-5 text-xl font-semibold text-white hover:from-phacil-gradient-orange hover:to-phacil-gradient-red";
      case "secondary":
        return "rounded bg-white px-8 py-4 text-xl font-semibold text-phacil-blue border-2 border-phacil-blue hover:text-white hover:bg-phacil-blue";
      default:
        return "";
    }
  };

  return (
    <button className={getButtonStyle()} onClick={handleClickButton}>
      {content}
    </button>
  );
};
