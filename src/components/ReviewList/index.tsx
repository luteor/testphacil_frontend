"use client";

import { useEffect, useState } from "react";

import { ReviewCard } from "../ReviewCard";
import { ReviewControlPanel } from "../ReviewControlPanel";

export type Review = {
  customerName: string;
  date: string;
  feedback: string;
  id: string;
  productName: string;
  rating: number;
};

type ReviewListProps = {
  reviews: Review[];
};

export const ReviewList = ({ reviews }: ReviewListProps) => {
  const [reviewFilters, setReviewFilters] = useState<number[]>([]); // Annoter le type de state comme un tableau de nombres
  const [filteredReviews, setFilteredReviews] = useState<Review[]>(reviews); // Annoter le type de state comme un tableau de Review
  const [sortReviews, setSortReviews] = useState<string>("Les plus récents"); // Annoter le type de state comme une union de chaînes de caractères

  useEffect(() => {
    let updatedReviews = reviews;

    if (reviewFilters.length) {
      updatedReviews = reviews.filter((review: Review) =>
        reviewFilters.includes(review.rating),
      );
    }

    const sortedReviews = [...updatedReviews].sort((a, b) => {
      const dateA = new Date(a.date);
      const dateB = new Date(b.date);
      const comparison =
        sortReviews === "Les plus récents" || sortReviews === "Les plus utiles"
          ? dateB.getTime() - dateA.getTime()
          : dateA.getTime() - dateB.getTime();
      return comparison;
    });

    setFilteredReviews(sortedReviews);
  }, [reviewFilters, sortReviews, reviews]);

  const overallRating =
    reviews.reduce((total, review) => total + review.rating, 0) /
    reviews.length;

  return (
    <div className="flex h-lvh flex-row items-start gap-10 p-20">
      <ReviewControlPanel
        overallRating={overallRating}
        reviewFilters={reviewFilters}
        reviews={reviews}
        setReviewFilters={setReviewFilters}
        setSortReviews={setSortReviews}
        totalReviews={reviews.length}
      />
      <div className="flex-grow">
        <div className="flex flex-row items-end justify-between">
          <h1 className="text-3xl font-semibold text-phacil-blue">
            Les évaluations de la boutique en ligne Phacil
          </h1>
          <span className="text-sm text-black">1 à 10 de 50 avis</span>
        </div>
        <div className="flex flex-col gap-10 py-8">
          {filteredReviews.map((review) => (
            <ReviewCard
              content={review.feedback}
              customer={review.customerName}
              date={review.date}
              initials={review.customerName
                .split(" ")
                .map((word) => word.charAt(0))
                .join("")} // Obtenir les initiales du client
              key={review.id}
              product={review.productName}
              rating={review.rating}
            />
          ))}
        </div>
      </div>
    </div>
  );
};
