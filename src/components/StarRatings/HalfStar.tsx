import Image from "next/image";

export const HalfStar = () => {
  return (
    <div className="stroke-red-500">
      <Image alt="star rating" height="35" src="/half-star.svg" width="35" />
    </div>
  );
};
