import { EmptyStar } from "./EmptyStar";
import { HalfStar } from "./HalfStar";
import { Star } from "./Star";

type StarRatingsProps = {
  rating: number;
};

export const StarRatings = ({ rating }: StarRatingsProps) => {
  const renderStars = () => {
    const stars = [];
    const integerRating = Math.floor(rating);
    const hasHalfStar = rating - integerRating !== 0;

    for (let i = 1; i <= 5; i++) {
      if (i <= integerRating) {
        stars.push(<Star key={i} />);
      } else if (i === integerRating + 1 && hasHalfStar) {
        stars.push(<HalfStar key={i} />);
      } else {
        stars.push(<EmptyStar key={i} />);
      }
    }

    return stars;
  };

  return (
    <div className="flex flex-row items-center gap-2">{renderStars()}</div>
  );
};
