import Image from "next/image";

export const Star = () => {
  return (
    <div className="stroke-red-500">
      <Image alt="star rating" height="35" src="/star.svg" width="35" />
    </div>
  );
};
