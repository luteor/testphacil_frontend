import Image from "next/image";

export const EmptyStar = () => {
  return (
    <div className="stroke-red-500">
      <Image alt="star rating" height="35" src="/empty-star.svg" width="35" />
    </div>
  );
};
