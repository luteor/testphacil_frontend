import { StarRatings } from "../StarRatings";
import { Avatar } from "./Avatar";

type ReviewCardHeaderProps = {
  customer: string;
  date: string;
  initials: string;
  rating: number;
};

export const ReviewCardHeader = ({
  customer,
  date,
  initials,
  rating,
}: ReviewCardHeaderProps) => {
  return (
    <div className="flex flex-row items-center justify-between">
      <div className="flex flex-row items-center gap-5">
        <Avatar initials={initials} />
        <div className="flex flex-col gap-1">
          <span className="text-2xl font-medium">{customer} </span>
          <div className="flex flex-row gap-1">
            <span className="text-md font-light text-phacil-slate">
              Avis publié le
            </span>
            <span className="text-md font-light text-phacil-slate underline">
              {date}
            </span>
          </div>
        </div>
      </div>
      <div className="flex flex-row items-center gap-2">
        <StarRatings rating={rating} />
        <span className="text-2xl font-semibold">{rating}/5</span>
      </div>
    </div>
  );
};
