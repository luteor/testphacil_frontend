type ReviewCardContentProps = {
  content: string;
};

export const ReviewCardContent = ({ content }: ReviewCardContentProps) => {
  return (
    <div className=" h-40 border-y-2 py-5">
      <p className="text-lg font-semibold text-phacil-blue">{content}</p>
    </div>
  );
};
