import { ReviewCardContent } from "./ReviewCardContent";
import { ReviewCardFooter } from "./ReviewCardFooter";
import { ReviewCardHeader } from "./ReviewCardHeader";

type ReviewCardProps = {
  content: string;
  customer: string;
  date: string; // Ou vous pouvez utiliser un type de date plus précis comme Date
  initials: string;
  product: string;
  rating: number; // Peut-être un type spécifique pour les évaluations, comme "Rating"
};

export const ReviewCard = ({
  content,
  customer,
  date,
  initials,
  product,
  rating,
}: ReviewCardProps) => {
  const reviewDate = new Date(date);

  const day = reviewDate.getDate();
  const monthIndex = reviewDate.getMonth();
  const year = reviewDate.getFullYear();

  const monthNames = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre",
  ];

  const formattedDate = `${day} ${monthNames[monthIndex]} ${year}`;
  return (
    <div className="flex flex-col justify-start gap-7 border-2 border-phacil-slate bg-white p-10">
      <ReviewCardHeader
        customer={customer}
        date={formattedDate}
        initials={initials}
        rating={rating}
      />
      <ReviewCardContent content={content} />
      <ReviewCardFooter product={product} />
    </div>
  );
};
