import { Button } from "../Button";

type ReviewCardFooterProps = {
  product: string;
};

export const ReviewCardFooter = ({ product }: ReviewCardFooterProps) => {
  return (
    <div className="flex flex-row items-center justify-between">
      <div className="flex flex-row gap-1">
        <span className="text-md font-light text-phacil-slate">
          Produit acheté :
        </span>
        <span className="text-md font-light text-phacil-sky">{product}</span>
      </div>
      <div className="flex flex-row items-center gap-6">
        <span className="text-md font-light text-phacil-slate">
          Cet avis vous a été utile ?
        </span>
        <Button content="Oui" variant="secondary" />
        <Button content="Non" variant="secondary" />
      </div>
    </div>
  );
};
