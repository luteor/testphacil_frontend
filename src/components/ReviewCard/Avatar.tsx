type AvatarProps = {
  initials: string;
};

export const Avatar = ({ initials }: AvatarProps) => {
  return (
    <div className="flex h-20 w-20 items-center justify-center rounded-full bg-phacil-gray p-2">
      <span className="text-3xl font-bold text-black">{initials}</span>
    </div>
  );
};
