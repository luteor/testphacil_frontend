type ButtonRoundedProps = {
  content: string;
  handleButtonClick: (content: string) => void;
  variant: "primary" | "secondary";
};

export const ButtonRounded = ({
  content,
  handleButtonClick,
  variant,
}: ButtonRoundedProps) => {
  const getButtonStyle = () => {
    switch (variant) {
      case "primary":
        return "bg-phacil-zinc border-phacil-slate text-phacil-slate rounded-full border-2 px-3 py-2 text-xs font-semibold";
      case "secondary":
        return "bg-phacil-zinc hover:border-phacil-slate text-phacil-slate rounded-full hover:border-2 px-3 py-2 text-xs font-semibold";
      default:
        return "";
    }
  };

  return (
    <button
      className={getButtonStyle()}
      onClick={() => handleButtonClick(content)}
    >
      {content}
    </button>
  );
};
