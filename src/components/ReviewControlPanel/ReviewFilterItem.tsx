import { Star } from "../StarRatings/Star";

type ReviewFilterItemProps = {
  percentage: number;
  reviewFilters: number[];
  setReviewFilters: React.Dispatch<React.SetStateAction<number[]>>;
  value: number;
};

export const ReviewFilterItem = ({
  percentage,
  reviewFilters,
  setReviewFilters,
  value,
}: ReviewFilterItemProps) => {
  const isChecked = reviewFilters.includes(value);

  const handleFilterCheckBoxChange = () => {
    if (!isChecked) {
      setReviewFilters([...reviewFilters, value]);
    } else {
      const newReviewFilters = reviewFilters.filter(
        (filter: number) => filter !== value,
      );
      setReviewFilters(newReviewFilters);
    }
  };

  return (
    <div className="flex flex-grow flex-row items-center justify-start gap-1">
      <input
        checked={isChecked} // Définir l'état de la case à cocher en fonction de isChecked
        id={`checkbox-${value}`} // Utiliser une ID unique pour chaque case à cocher
        name={`checkbox-${value}`} // Utiliser un nom unique pour chaque case à cocher
        onChange={handleFilterCheckBoxChange}
        type="checkbox"
        value={value}
      />
      <label
        className="h-auto w-3 text-xl text-phacil-blue"
        htmlFor={`checkbox-${value}`}
      >
        {value.toString()}
      </label>
      <div className="h-auto w-4">
        <Star />
      </div>
      <div className="h-2.5 w-44 rounded-full bg-phacil-gray">
        <div
          className={`h-2.5 rounded-full bg-phacil-blue`}
          style={{ width: `${percentage}%` }}
        ></div>
      </div>
      <span className="text-phacil-blue">{percentage} %</span>
    </div>
  );
};
