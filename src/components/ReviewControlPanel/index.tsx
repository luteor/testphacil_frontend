import { Button } from "../Button";
import { Review } from "../ReviewList";
import { OverallReviewScore } from "./OverallReviewScore";
import { ReviewFilter } from "./ReviewFilter";
import { ReviewSorter } from "./ReviewSorter";

type ReviewControlPanelProps = {
  overallRating: number;
  reviewFilters: number[];
  reviews: Review[];
  setReviewFilters: React.Dispatch<React.SetStateAction<number[]>>;
  setSortReviews: React.Dispatch<React.SetStateAction<string>>;
  totalReviews: number;
};

export const ReviewControlPanel = ({
  overallRating,
  reviewFilters,
  reviews,
  setReviewFilters,
  setSortReviews,
  totalReviews,
}: ReviewControlPanelProps) => {
  const handleResetFiltersButtonClick = () => {
    setReviewFilters([]);
  };

  return (
    <div className=" flex w-80 flex-col items-center gap-10 bg-white px-5 py-10 shadow">
      <OverallReviewScore
        overallRating={overallRating}
        totalReviews={totalReviews}
      />
      <div className="flex flex-col gap-5 ">
        <Button content="Écrire un avis" variant="primary" />
        <Button
          content="Voir tous les avis"
          handleClickButton={handleResetFiltersButtonClick}
          variant="secondary"
        />
      </div>

      <ReviewFilter
        reviewFilters={reviewFilters}
        reviews={reviews}
        setReviewFilters={setReviewFilters}
      />

      <ReviewSorter setSortReviews={setSortReviews} />
    </div>
  );
};
