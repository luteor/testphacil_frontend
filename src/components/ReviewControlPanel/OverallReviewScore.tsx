import { StarRatings } from "../StarRatings";

type OverallReviewScoreProps = {
  overallRating: number;
  totalReviews: number;
};

export const OverallReviewScore = ({
  overallRating,
  totalReviews,
}: OverallReviewScoreProps) => {
  return (
    <div className="flex flex-col items-center gap-5">
      <span className="text-4xl font-semibold text-phacil-blue">
        {overallRating} / 5
      </span>
      <StarRatings rating={overallRating} />
      <span className="text-xl text-phacil-blue ">
        Total : {totalReviews} avis
      </span>
    </div>
  );
};
