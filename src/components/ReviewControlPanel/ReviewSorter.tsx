import { useState } from "react";

import { ButtonRounded } from "../ButtonRounded";

type ReviewSorterProps = {
  setSortReviews: React.Dispatch<React.SetStateAction<string>>;
};

export const ReviewSorter = ({ setSortReviews }: ReviewSorterProps) => {
  const [activeButton, setActiveButton] = useState("Les plus récents");

  const handleButtonClick = (sort: string) => {
    setSortReviews(sort);
    setActiveButton(sort);
  };

  return (
    <div className="flex flex-grow flex-col items-center gap-5">
      <span className="text-xl font-medium text-phacil-blue">
        Trier les avis
      </span>
      <div className="flex flex-row flex-wrap items-center justify-center gap-4">
        <ButtonRounded
          content="Les plus récents"
          handleButtonClick={() => handleButtonClick("Les plus récents")}
          variant={
            activeButton === "Les plus récents" ? "primary" : "secondary"
          }
        />
        <ButtonRounded
          content="Les plus utiles"
          handleButtonClick={() => handleButtonClick("Les plus utiles")}
          variant={activeButton === "Les plus utiles" ? "primary" : "secondary"}
        />
        <ButtonRounded
          content="Les moins récents"
          handleButtonClick={() => handleButtonClick("Les moins récents")}
          variant={
            activeButton === "Les moins récents" ? "primary" : "secondary"
          }
        />
        <ButtonRounded
          content="Les moins utiles"
          handleButtonClick={() => handleButtonClick("Les moins utiles")}
          variant={
            activeButton === "Les moins utiles" ? "primary" : "secondary"
          }
        />
      </div>
    </div>
  );
};
