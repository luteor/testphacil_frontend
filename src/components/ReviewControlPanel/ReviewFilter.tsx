import { ReviewFilterItem } from "./ReviewFilterItem";

type Review = {
  rating: number;
};

type ReviewFilterProps = {
  reviewFilters: number[];
  reviews: Review[];
  setReviewFilters: React.Dispatch<React.SetStateAction<number[]>>;
};

export const ReviewFilter = ({
  reviewFilters,
  reviews,
  setReviewFilters,
}: ReviewFilterProps) => {
  const countReviewsByRating = (rating: number) => {
    return reviews.filter((review) => review.rating === rating).length;
  };

  const calculatePercentage = (rating: number) => {
    const count = countReviewsByRating(rating);
    const totalReviews = reviews.length;
    return Math.round((count / totalReviews) * 100);
  };

  return (
    <div className="flex flex-grow flex-col gap-5 ">
      <ReviewFilterItem
        percentage={calculatePercentage(5)}
        reviewFilters={reviewFilters}
        setReviewFilters={setReviewFilters}
        value={5}
      />
      <ReviewFilterItem
        percentage={calculatePercentage(4)}
        reviewFilters={reviewFilters}
        setReviewFilters={setReviewFilters}
        value={4}
      />
      <ReviewFilterItem
        percentage={calculatePercentage(3)}
        reviewFilters={reviewFilters}
        setReviewFilters={setReviewFilters}
        value={3}
      />
      <ReviewFilterItem
        percentage={calculatePercentage(2)}
        reviewFilters={reviewFilters}
        setReviewFilters={setReviewFilters}
        value={2}
      />
      <ReviewFilterItem
        percentage={calculatePercentage(1)}
        reviewFilters={reviewFilters}
        setReviewFilters={setReviewFilters}
        value={1}
      />
    </div>
  );
};
