## Installation
- Clonez le projet avec `git clone`.
- Lancez le projet avec `npm run dev`.

Attention aux versions utilisées :
- node : 20.11.1
- npm : 10.2.4

## Description
Nous voudrions tester vos capacités en développement front. Pour cela, nous vous proposons un test d'aptitude.

Pour ce faire, nous vous demandons de forker ce projet, de le compléter, puis de nous partager votre dépôt accompagné du fichier Figma par mail à joan@phacil.delivery. Nous ferons un debrief lors de votre prochain entretien.

Dans ce projet se trouve un fichier de seed qui enregistre toutes les notes et avis reçus par la plateforme [phacil.delivery](phacil.delivery).

Votre rôle est de concevoir, sur Figma, puis de coder dans le projet, une page d'avis.

Le choix du design est libre. Nous vous proposons, dans la section ressources, la liste de nos plateformes pour pouvoir vous inspirer (si besoin).

L'architecture et les éléments du projet peuvent être totalement changés à votre guise, aucune restriction ne vous est imposée à part celle de rester sur Next.js.

## Partie Obligatoire
- Complétez un projet design sur Figma.
- Une page d'avis basée sur le fichier seed du projet.

## Partie Bonus
- Tout autre design sur Figma en lien avec ce projet serait intéressant.
- Peut-être que d'autres pages au sein du site, comme une page d'accueil, un catalogue, une page panier, pourraient être intéressantes.

## Ressources
- [phacil.delivery](phacil.delivery)
- [phacil.app](https://www.phacil.app/)


### Date de rendu : Jeudi 7 Mars
