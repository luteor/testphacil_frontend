import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  plugins: [],
  theme: {
    extend: {
      colors: {
        "phacil-blue": "#191A6F",
        "phacil-gray": "#B7B8C5",
        "phacil-sky": "#017BFE",
        "phacil-slate": "#636485",
        "phacil-zinc": "#FBFBFF",
      },
      fontFamily: {
        sans: ["var(--font-sf-pro-text)"],
      },
      gradientColorStops: {
        "phacil-gradient": {
          orange: "#FFAB66", // blue-500
          red: "#FE665E", // cyan-500
        },
      },
    },
  },
};
export default config;
